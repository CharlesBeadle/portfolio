import React from 'react'
import { IconContext } from 'react-icons'

import '../css/links.css'
import { FaCode } from 'react-icons/fa'
import { FaTwitter } from 'react-icons/fa'

const Links = () => (
  <IconContext.Provider
    value={{
      style: {
        width: '50px',
        height: '50px',
        color: '#e4e7eb',
      },
    }}
  >
    <div className="linksDiv">
      <a
        rel="noopener noreferrer"
        href="https://bitbucket.org/CharlesBeadle/portfolio/src"
        title="Show me the code"
        target="_blank"
      >
        <div id="linksBucketIcon">
          <FaCode />
        </div>
      </a>
      <a
        rel="noopener noreferrer"
        title="See what I'm doing on Twitter"
        href="https://twitter.com/marinus_11"
        target="_blank"
      >
        <div id="linksTwitterIcon">
          <FaTwitter />
        </div>
      </a>
    </div>
  </IconContext.Provider>
)

export default Links
