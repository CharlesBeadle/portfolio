import React from 'react'

import '../css/footer.css'
import { IconContext } from 'react-icons'
import { FaCode } from 'react-icons/fa'

const footerStyle = {
  fontFamily: 'cooper hewitt',
}

const Footer = () => (
  <IconContext.Provider
    value={{
      style: {
        width: '30px',
        height: '30px',
        color: '#cbd2d9',
      },
    }}
  >
    <div className="footerDiv">
      <div id="footerSub1">
        <FaCode />
        <p style={footerStyle}>with</p>
        <a
          style={footerStyle}
          target="_blank"
          href="https://www.gatsbyjs.org/"
          rel="noopener noreferrer"
        >
          Gatsby
        </a>
      </div>

      <div id="footerSub3">
        <p style={footerStyle}>786-423-1789</p>
      </div>
    </div>
  </IconContext.Provider>
)

export default Footer
