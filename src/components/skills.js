import React from 'react'

import '../css/skills.css'

const Skills = () => (
  <div id="skillsWrapper">
    <div id="skillsDivParaContainer">
      <div id="skillsExperience">
        <h2 className="skillsHeading">I'm experienced with:</h2>
        <p>HTML</p>
        <p>CSS</p>
        <p>JavaScript</p>
        <p>React</p>
        <p>Gatsby</p>
      </div>
      <div id="skillsLearning">
        <h2 className="skillsHeading">Learning:</h2>
        <p>React Hooks</p>
        <p>Redux</p>
        <p>Node/Express</p>
        <p>MongoDB</p>
      </div>
      <div id="skillsOther">
        <h2 className="skillsHeading">Other skills:</h2>
        <p>Responsive Design</p>
        <p>Git</p>
        <p>Dev Tools</p>
        <p>Command-line</p>
        <p>GraphQL Queries</p>
        <p>Headless CMS</p>
        <p>Styled Components</p>
        <p>Netlify</p>
      </div>
      <div id="skillsEducation">
        <h2 className="skillsHeading">Education:</h2>
        <p>B.A. FIU</p>
        <p>Political Science</p>
      </div>
    </div>
  </div>
)

export default Skills
