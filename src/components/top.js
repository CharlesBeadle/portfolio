import React from 'react'

import 'typeface-cooper-hewitt'
import '../css/top.css'
import Links from './links'

const Top = () => (
  <div className="topDiv">
    <Links />
    <div id="topGreeting">
      <h1>Hey, I'm Charles</h1>
      <p>I'm a front-end developer from Tampa.</p>

      <p>
        I've spent most of my time working in sales, but my passion for the last
        3 years has been web development. I spend the vast majority of my free
        time acquiring new knowledge and skills.
      </p>
      <p>
        I look forward to bringing resourcefulness, hard work, loyalty and other
        value to the company that will give me the opportunity to launch my
        career.
      </p>
    </div>
  </div>
)

export default Top
