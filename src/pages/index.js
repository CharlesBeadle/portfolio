import React from 'react'

import Layout from '../components/layout'
import SEO from '../components/seo'
import Top from '../components/top'
import Bottom from '../components/bottom'
import Footer from '../components/footer'
import Description from '../components/description'
import Skills from '../components/skills'

const IndexPage = () => (
  <Layout>
    <SEO
      keywords={[
        `Charles`,
        `Beadle`,
        `Portfolio`,
        `React`,
        `Gatsby`,
        `GraphQL`,
      ]}
    />
    <Top />
    <Description />
    <Bottom />
    <Skills />
    <Footer />
  </Layout>
)

export default IndexPage
